#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif
#include <pthread.h>

struct
{
  pthread_spinlock_t testlock;
  char pad[64 - sizeof (pthread_spinlock_t)];
} test __attribute__((aligned(64)));

static void
__attribute__((constructor))
init_spin (void)
{
  pthread_spin_init (&test.testlock, 0);
}

static void work_todo (void);

static inline void
do_work (void)
{
  pthread_spin_lock(&test.testlock);
  work_todo ();
  pthread_spin_unlock(&test.testlock);
}

#include "tst-variable-precise-overhead-skeleton.c"
