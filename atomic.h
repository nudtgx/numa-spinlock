#define atomic_store_release(ptr, val) \
  __atomic_store_n ((ptr), (val), __ATOMIC_RELEASE)

#define atomic_store_relaxed(ptr, val) \
  __atomic_store_n ((ptr), (val), __ATOMIC_RELAXED)

#define atomic_load_acquire(ptr) \
  __atomic_load_n ((ptr), __ATOMIC_ACQUIRE)

#define atomic_load_relaxed(ptr) \
  __atomic_load_n ((ptr), __ATOMIC_RELAXED)

#define atomic_exchange_acquire(ptr, val) \
  __atomic_exchange_n ((ptr), (val), __ATOMIC_ACQUIRE)

# define atomic_compare_exchange_weak_acquire(mem, expected, desired) \
  __atomic_compare_exchange_n ((mem), (expected), (desired), 1,	      \
			       __ATOMIC_ACQUIRE, __ATOMIC_RELAXED)

#define atomic_compare_and_exchange_val_acq(mem, newval, oldval) \
  __sync_val_compare_and_swap ((mem), (oldval), (newval))

#define atomic_compare_and_exchange_bool_acq(mem, newval, oldval) \
  (! __sync_bool_compare_and_swap ((mem), (oldval), (newval)))

#define atomic_spin_nop() asm ("rep; nop")

/* We don't use mfence because it is supposedly slower due to having to
   provide stronger guarantees (e.g., regarding self-modifying code).  */
#define atomic_full_barrier() \
    __asm __volatile ("lock; orl $0, (%%rsp)" ::: "memory")
#define atomic_read_barrier() __asm ("" ::: "memory")
#define atomic_write_barrier() __asm ("" ::: "memory")

#define ATOMIC_EXCHANGE_USES_CAS 0

#ifndef __glibc_unlikely
# define __glibc_unlikely(cond)	__builtin_expect ((cond), 0)
#endif
