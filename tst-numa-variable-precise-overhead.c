#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif
#include <atomic.h>
#include "numa-spinlock.h"

struct numa_spinlock *lock;

struct work_todo_argument
{
  unsigned long *v1;
  unsigned long *v2;
  unsigned long *v3;
  unsigned long *v4;
};

static void *
work_todo (void *v)
{
  struct work_todo_argument *p = v;
  unsigned long ret;
  *p->v1 = *p->v1 + 1;
  *p->v2 = *p->v2 + 1;
  ret = __sync_val_compare_and_swap (p->v4, 0, 1);
  *p->v3 = *p->v3 + ret;
  return (void *) 2;
}

static inline void
do_work (struct numa_spinlock_info *lock_info)
{
  numa_spinlock_apply (lock_info);
  while (numa_spinlock_pending (lock_info))
    atomic_spin_nop ();
}

#define USE_NUMA_SPINLOCK
#include "tst-variable-precise-overhead-skeleton.c"
