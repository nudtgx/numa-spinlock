/* Copyright (C) 2019 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

#ifndef _NUMA_SPINLOCK_H
#define _NUMA_SPINLOCK_H

#include <features.h>

__BEGIN_DECLS

/* The NUMA spinlock.  */
struct numa_spinlock;

/* The NUMA spinlock information for each thread.  */
struct numa_spinlock_info
{
  /* The workload function of this thread.  */
  void *(*workload) (void *);
  /* The argument pointer passed to the workload function.  */
  void *argument;
  /* The return value of the workload function.  */
  void *result;
  /* The pointer to the NUMA spinlock.  */
  struct numa_spinlock *lock;
  /* The next thread on the local NUMA spinlock thread list.  */
  struct numa_spinlock_info *next;
  /* The NUMA node number.  */
  unsigned int node;
  /* Non-zero to indicate that the thread wants the NUMA spinlock.  */
  int pending;
  /* Reserved for future use.  */
  void *__reserved[4];
};

/* Return a pointer to a newly allocated NUMA spinlock.  */
extern struct numa_spinlock *numa_spinlock_alloc (void);

/* Free the memory space of the NUMA spinlock.  */
extern void numa_spinlock_free (struct numa_spinlock *);

/* Initialize the NUMA spinlock information block.  */
extern int numa_spinlock_init (struct numa_spinlock *,
			       struct numa_spinlock_info *);

/* Apply and wait for the NUMA spinlock with a NUMA spinlock information
   block.  */
extern void numa_spinlock_apply (struct numa_spinlock_info *);

/* Apply for the non-blocking NUMA spinlock with a NUMA spinlock
   information block.  */
extern void numa_spinlock_apply_nonblock (struct numa_spinlock_info *);

/* Non-zero if the NUMA spinlock is pending.  */
extern int numa_spinlock_pending (struct numa_spinlock_info *);

__END_DECLS

#endif /* numa-spinlock.h */
