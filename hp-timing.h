#define HP_TIMING_AVAIL	(1)

typedef unsigned long long int hp_timing_t;
#define HP_TIMING_NOW(Var) \
  (__extension__ ({				\
    unsigned int __aux;				\
    (Var) = __builtin_ia32_rdtscp (&__aux);	\
  }))

#define HP_TIMING_DIFF(Diff, Start, End) ((Diff) = (End) - (Start))
