#ifndef _PTHREAD_MCS_SPINLOCK_H
#define _PTHREAD_MCS_SPINLOCK_H

#include <features.h>

__BEGIN_DECLS

typedef struct
{
  volatile int spin_lock;
  void *mcs_lock;
} mcs_spinlock_t;

extern int mcs_spin_destroy (mcs_spinlock_t *);
extern int mcs_spin_init (mcs_spinlock_t *, int);
extern int mcs_spin_lock (mcs_spinlock_t *);
extern int mcs_spin_trylock (mcs_spinlock_t *);
extern int mcs_spin_unlock (mcs_spinlock_t *);

__END_DECLS

#endif /* mcs_spinlock.h */
